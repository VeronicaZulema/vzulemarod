package SistemaNomina;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class EliminarEmpleados extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNuevo;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JLabel lblNmero;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JRadioButton rdbtnChofer;
	private JRadioButton rdbtnCargador;
	private JRadioButton rdbtnAuxiliar;
	private JLabel lblTipo;
	private JRadioButton rdbtnInterno;
	private JRadioButton rdbtnExterno;
	private JButton btnOk;
	private JButton btnCancel;
	private JButton btnBuscar_1;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JScrollPane scrollPane_3;
	
	public static String nombre;
	public static int chofer;
	public static int cargador;
	public static int auxiliar;
	public static int tipoInt;
	public static int tipoExt;
	public static int contEmp;
	public static int contAce;
	private ButtonGroup agrupadorRol;//Agrupador para los RadioButton
	private ButtonGroup agrupadorTipo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarEmpleados frame = new EliminarEmpleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarEmpleados() {
		setTitle("Eliminar empleado");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 474, 369);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Empleados EliminarEmpleados= new Empleados();
				EliminarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnNuevo.setBounds(27, 22, 70, 23);
		contentPane.add(btnNuevo);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuscarEmpleados EliminarEmpleados= new BuscarEmpleados();
				EliminarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnBuscar.setBounds(96, 22, 83, 23);
		contentPane.add(btnBuscar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Modificar_empleados EliminarEmpleados= new Modificar_empleados();
				EliminarEmpleados.setVisible(true);
				dispose();
			}
		});
		btnModificar.setBounds(178, 22, 98, 23);
		contentPane.add(btnModificar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(275, 22, 89, 23);
		contentPane.add(btnEliminar);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if ((c < '0' || c > '9') && ((c != KeyEvent.VK_BACK_SPACE))) {
					e.consume();
					JOptionPane.showMessageDialog(null, "Ingresa solo numeros",
							"ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		textField.setBounds(78, 82, 101, 29);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblNmero = new JLabel("N\u00FAmero:");
		lblNmero.setBounds(27, 72, 63, 39);
		contentPane.add(lblNmero);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(27, 102, 63, 39);
		contentPane.add(lblNombre);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(78, 113, 319, 28);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		lblRol = new JLabel("Rol:");
		lblRol.setBounds(48, 157, 63, 39);
		contentPane.add(lblRol);
		
		rdbtnChofer = new JRadioButton("Chofer");
		rdbtnChofer.setEnabled(false);
		rdbtnChofer.setBounds(78, 166, 109, 23);
		contentPane.add(rdbtnChofer);
		
		rdbtnCargador = new JRadioButton("Cargador");
		rdbtnCargador.setEnabled(false);
		rdbtnCargador.setBounds(78, 192, 109, 23);
		contentPane.add(rdbtnCargador);
		
		rdbtnAuxiliar = new JRadioButton("Auxiliar");
		rdbtnAuxiliar.setEnabled(false);
		rdbtnAuxiliar.setBounds(78, 218, 109, 23);
		contentPane.add(rdbtnAuxiliar);
		
		lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(232, 157, 63, 39);
		contentPane.add(lblTipo);
		
		rdbtnInterno = new JRadioButton("Interno");
		rdbtnInterno.setEnabled(false);
		rdbtnInterno.setBounds(268, 166, 109, 23);
		contentPane.add(rdbtnInterno);
		
		rdbtnExterno = new JRadioButton("Externo");
		rdbtnExterno.setEnabled(false);
		rdbtnExterno.setBounds(268, 192, 109, 23);
		contentPane.add(rdbtnExterno);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 146, 166, 112);
		contentPane.add(scrollPane_1);
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(213, 146, 182, 112);
		contentPane.add(scrollPane_2);
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 72, 428, 200);
		contentPane.add(scrollPane_3);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 428, 50);
		contentPane.add(scrollPane);
		
		btnOk = new JButton("Eliminar");
		btnOk.setEnabled(false);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String NumEmp;
				
				NumEmp="";
				
				NumEmp = textField.getText(); //almacena en una variable el numero del empleado que se desea eliminar.
				
				if (JOptionPane.showConfirmDialog(rootPane, "Se eliminar� el registro, �desea continuar?",
				        "Eliminar Registro", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					EliminarEmp(NumEmp);  //Manda llamar el metodo para eliminar el empleado de la base de datos.
					limpiar();
				}
			}
		});
		btnOk.setBounds(250, 297, 89, 23);
		contentPane.add(btnOk);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancel.setBounds(349, 297, 89, 23);
		contentPane.add(btnCancel);
		
		agrupador();
		
		btnBuscar_1 = new JButton("Buscar");
		btnBuscar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Metodo que se ejecuta cuando se le da clic al boton de buscar.
				String numemp;

				numemp="";
				contEmp=0;
				
				contAce++;
				
				if (contAce>1){
					limpiar();
				}
				btnOk.setEnabled(true);
				
				numemp=textField.getText();  //Almacena en una variable el numero del empleado.
				buscarEmp(numemp);  //Manda llamar el metodo para verificar que el empleado que se quiere eliminar se encuentra en la base de datos.
				
				if (contEmp==0){
					JOptionPane.showMessageDialog(null,"Empleado no registrado");
					inicializarvar();
					limpiar();
					btnOk.setEnabled(false);
				}				
				textField_1.setText(nombre);
				
				//Dependiendo del valor que se obtengan las siguientes variables de la base de datos se activa o desactivan los radiobutton.
				if (chofer == 1){
					rdbtnChofer.setSelected(true);
				}
				if(cargador == 1 ){
					rdbtnCargador.setSelected(true);
				}
				 if(auxiliar == 1 ){
					rdbtnAuxiliar.setSelected(true);
				}
				 if(tipoInt == 1 ){
					rdbtnInterno.setSelected(true);
				}
				if(tipoExt == 1 ){
					rdbtnExterno.setSelected(true);
				}
			}
		});
		btnBuscar_1.setBounds(151, 297, 89, 23);
		contentPane.add(btnBuscar_1);
	}
	private void buscarEmp(String numemp){
		//Metodo donde se obtiene la informacion del empleado
		MyDataAcces conexion = new MyDataAcces(); //Instancia la clase donde se encuentra la conexion para la BD.
		ResultSet resultado;   //Variable para obtener los resultados de la consulta.
		
		try {
		resultado = conexion.getQuery("select Nombre,Chofer,Cargador,Auxiliar,tipoInt,tipoExt from empleados where Numero="+numemp+"");
			while(resultado.next()){
				//Almacena los valores que trajo la consulta en las variables.
				nombre=resultado.getString("Nombre");
				chofer=resultado.getInt("Chofer");
				cargador=resultado.getInt("Cargador");
				auxiliar=resultado.getInt("Auxiliar");
				tipoInt=resultado.getInt("tipoInt");
				tipoExt=resultado.getInt("tipoExt");
				
				contEmp++;     //Variable para saber si existe el empleado que se desea buscar.
			}
		} catch (SQLException a) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,"Error : Al consultar el empleado","Mensaje",JOptionPane.ERROR_MESSAGE);
		}
	}
	public void EliminarEmp(String NumEmp){
		//Metodo para eliminar el empleado.
		MyDataAcces conexion = new MyDataAcces();
		
		try{
		conexion.setQuery
		("DELETE FROM `empleados` WHERE  `Numero`='"
				+ NumEmp + "'");
		}catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Error : Al eliminar empleado","Mensaje",JOptionPane.ERROR_MESSAGE);
	    }
	}	
	public void limpiar(){
		//Metodo para limpiar los cuadros de texto.
		textField_1.setText("");

		agrupadorRol.remove(rdbtnChofer);
		agrupadorRol.remove(rdbtnCargador);
		agrupadorRol.remove(rdbtnAuxiliar);
		
		agrupadorTipo.remove(rdbtnExterno);
		agrupadorTipo.remove(rdbtnInterno);
		
		rdbtnChofer.setSelected(false);
		rdbtnCargador.setSelected(false);
		rdbtnAuxiliar.setSelected(false);
		rdbtnInterno.setSelected(false);	
		rdbtnExterno.setSelected(false);	
		
		agrupador();
				
	}
	public void agrupador(){
		//Metodo para agrupar los radiobutton
		agrupadorRol=new ButtonGroup();
		agrupadorRol.add(rdbtnChofer);
		agrupadorRol.add(rdbtnCargador);
		agrupadorRol.add(rdbtnAuxiliar);
		
		agrupadorTipo=new ButtonGroup();
		agrupadorTipo.add(rdbtnInterno);
		agrupadorTipo.add(rdbtnExterno);
		
	}
	public void inicializarvar(){
		//Metodo para inicializar las variables.
		nombre="";
		chofer=0;
		cargador=0;
		auxiliar=0;
		tipoInt=0;
		tipoExt=0;
		contEmp=0;
		contAce=0;
		
	}
}
